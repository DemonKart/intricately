# README

I was not able to complete the test. At the end I wrote some [key takeaways](#after-tests-considerations).
However, below are the instructions on how to build run what I got:

## Running what is available

### dependencies

- 2.7.2
- docker
- docker-compose

### Running the application dependencies

```shell
$ docker-compose up -d
```

### Database initialization

If it is the first time you are running the application, create, migrate and populate
the DB by running the following command:

```shell
$ rails db:setup
```

### How to run the test suite
The only tests available are for testing the http code returns... Normally I would
do a model tests for testing business rules, such as data transformation, conditional
branching, etc and add integration tests (requests) to test the inputs and outputs of each
route.

```
$ bundle exec rspec
```

### Running the application

```
$ rails s
```

### Calling the api
There is a [api.http](/api.http) file here that may be used to run the requests if you are
using vscode + restclient extension or IntelliJ. If that is not the case, here are the
equivalent curl commands:

```curl
$ curl --request GET \
    --url 'http://localhost:3000/dns_records.json?page=1&included%5B%5D=ipsum.com&included%5B%5D=dolor.com&excluded%5B%5D=sit.com' \
    --header 'user-agent: vscode-restclient'

$ curl --request POST \
  --url http://localhost:3000/dns_records.json \
  --header 'content-type: application/json' \
  --header 'user-agent: vscode-restclient' \
  --data '{"dns_records": {"ip": "1.1.1.1","hostnames_attributes": [{"hostname": "lorem.com"},{"hostname": "ipsum.com"}]}}'

```

## After tests considerations
I was a little rusty on configuring docker-compose to host the image of the application.
I included the Dockerfile to build it, but I decided to use only the database using
docker-compose so that when files are created, I do not need to keep changing file owner
in the host machine. Also I would consider something closer to and `alpine` version of ruby,
but then I would need to do some fine tuning on the dependencies. For time reasons I decided
to go with the full version.

The biggest concern here was indeed the database. I was trying to go with the approach
to use postgresql for all it could handle. So I began trying to use a single table with
a string array of hostnames. I read in the documentations, as in the jounal.md file show,
that searching for values in array fields was a database misdesign. So I created another
branch, try to follow the 'hints' or 'tricks' in the test description, such as "IP has
many hostnames" and vice versa. This is usually how we state tables in relational
databases... I adapted the controller to handle dns records creation through IPs + nested
attributes. I works fine but creates another problem, redundant hostnames records.
I would need to adapt it to search for the records and associate them 'manually'. But it
does not seem right as well. So I remembered that we can create GIN indexes on JSONB data
columns, which might solve the issue of the first approach, but I have spent 12 hours on
this test already and I am not satisfied with the result yet...
Another thing that would simplify the implementation would be if the the primary keys
of `ips` and `hostnames` were their actual values. This would enable to use a upsert_all
dns_records, one request would fan into #hostnames records to be used in the upsert.

Also, I realised that elastic search seems to be a better tool for the job.
It shines with the ability of adding records with array of strings and searching for them
as tags using the match operator.

## Conslusions
I got a lot of bumps on the road during the test, due to have not being coding in rails
for a log time. Nevertheless, I actually really enjoyed doing this test since it asks for
a simple API, with great OpenAPI specs and at the same time it shows a "real world
scenario". Usually I like to discuss with the team about problems like this one, maybe
someone would have had more experience with elastic search and go straight to it.