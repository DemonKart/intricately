class AddIpsAndHostnameToDnsRecords < ActiveRecord::Migration[6.1]
  def change
    add_reference :dns_records, :ip, foreign_key: true
    add_reference :dns_records, :hostname, foreign_key: true
  end
end
