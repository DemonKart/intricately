# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Ip.create({ ip_address: '1.1.1.1',
            hostnames_attributes: [{ name: 'lorem.com' }, { name: 'ipsum.com' }, { name: 'dolor.com' },
                                   { name: 'amet.com' }] })
Ip.create({ ip_address: '2.2.2.2', hostnames_attributes: [{ name: 'ipsum.com' }] })
Ip.create({ ip_address: '3.3.3.3',
            hostnames_attributes: [{ name: 'ipsum.com' }, { name: 'dolor.com' }, { name: 'amet.com' }] })
Ip.create({ ip_address: '4.4.4.4',
            hostnames_attributes: [{ name: 'ipsum.com' }, { name: 'dolor.com' }, { name: 'sit.com' },
                                   { name: 'amet.com' }] })
Ip.create({ ip_address: '5.5.5.5', hostnames_attributes: [{ name: 'dolor.com' }, { name: 'sit.com' }] })
