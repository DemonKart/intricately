Regarding the n:m relationship between `hostnames` and `dnss`, we could use a `:has_and_belongs_to_many` or `:has_many... :through`. The second one would create a controller and a model for the _strong relationship_ table `hostnames_dnss`, which would be better for when we want to further customize the relationship and its data.
I decided to choose the first option, since there are no further requirements for the relationship table other than mapping the initial tables entities.

---

Turns out I decided to represent the DNS records as a single entity for its simplicity and support by ActiveRecord. Now the hostnames are going to be an array.

---

I was wrong on the above assumptions :) as of postgresql docs: "Arrays are not sets; searching for specific array elements can be a sign of database misdesign. Consider using a separate table with a row for each item that would be an array element. This will be easier to search, and is likely to scale better for a large number of elements." So lets create `ips` and `hostnames` tables and a join table of them named dns_records