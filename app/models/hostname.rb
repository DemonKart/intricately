class Hostname < ApplicationRecord
  validates :name, presence: true
  has_many :dns_records
  has_many :ips, through: :dns_records

  def self.matching_records(ips_list, names)
    Hostname.joins(:dns_records)
      .where(dns_records: { ip_id: ips_list })
      .where.not(name: names)
      .group(:name)
      .count
  end
end
