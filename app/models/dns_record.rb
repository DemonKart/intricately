class DnsRecord < ApplicationRecord
  belongs_to :ip
  belongs_to :hostname
  accepts_nested_attributes_for :ip, :hostname

  def self.included_ips(names)
    DnsRecord.left_joins(:hostname, :ip)
      .where({ hostname: { name: names } })
      .group([:ip_id])
      .having('count(*) = ?', names.length)
      .select(:ip_id)
  end

  def self.excluded_ips(names)
    DnsRecord.left_joins(:hostname)
      .where({ hostname: { name: names } })
      .select(:ip_id)
  end
end
