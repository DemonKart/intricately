class Ip < ApplicationRecord
  validates :ip_address, presence: true
  has_many :dns_records
  has_many :hostnames, through: :dns_records
  accepts_nested_attributes_for :hostnames
end
