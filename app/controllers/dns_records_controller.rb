class DnsRecordsController < ApplicationController
  # GET /dns_records
  def index
    parsed_params = search_records_params
    included = DnsRecord.included_ips(parsed_params['included'])
    excluded = DnsRecord.excluded_ips(parsed_params['excluded'])

    # I was not able to find a subtraction of Arel that worked... sorry for the pluck ids here
    results_ip_list = included.pluck(:ip_id) - excluded.pluck(:ip_id)
    count_records = DnsRecord.where(ip_id: results_ip_list).count(:ip_id)
    records = Ip.joins(:dns_records).where(dns_records: { ip_id: results_ip_list }).select(:id, :ip_address).uniq
    names_to_excude = parsed_params['included'] + parsed_params['excluded']
    hostnames = Hostname.matching_records(results_ip_list, names_to_excude)

    @return_values = { total_records: count_records, records: records, related_hostnames: hostnames }

    render json: @return_values
  end

  # POST /dns_records
  def create
    parsed_params = dns_record_params
    # There is a uniqueness problem here. for every insertion operation, we may create redundant
    # Hostnames names... since upsert_all does not work with nested attributes,
    # I should change this simpler way of adding through nested attributes to insert_all
    # ips by addres and insert_all hostname by name.
    @ip_record = Ip.new(ip_data_from_dns_record_params(parsed_params))

    if @ip_record.save
      render json: { id: @ip_record.id }, status: :created
    else
      # This needs refactor. A better way of showing "dns_records" params errors
      # instead of ip_record on save errors.
      render json: @ip_record.errors, status: :unprocessable_entity
    end
  end

  private

  # Only allow a list of trusted parameters through.
  def dns_record_params
    params.require(:dns_records).permit([:ip, { hostnames_attributes: :hostname }])
  end

  # Since only Ip has_many hostnames, only it has access to Active Record nestes attributes
  # This IP creation is handled here to enable mantaining routes coherence
  def ip_data_from_dns_record_params(parsed_params)
    hostnames_items = parsed_params.to_hash['hostnames_attributes']
    hostnames = hostnames_items.collect { |item| { name: item['hostname'] } }
    { ip_address: parsed_params['ip'], hostnames_attributes: hostnames }
  end

  def search_records_params
    params.require(:page)
    params.permit([:page, { included: [], excluded: [] }])
  end
end
