require 'rails_helper'

RSpec.describe 'DnsRecords', type: :request do
  describe 'POST /dns_records.json' do
    payload = { dns_records: { ip: '1.1.1.1', hostnames_attributes: [{ hostname: 'lorem.test' }] } }
    subject { post('/dns_records.json', params: payload) }

    it 'creates a DnsRecord' do
      expect { subject }.to change(DnsRecord, :count).by(1)

      expect(response.content_type).to include('application/json')
      expect(response).to have_http_status(:created)
    end
  end

  describe 'GET /dns_records.json' do
    it 'returns success' do
      get('/dns_records.json?page=1')

      expect(response.content_type).to include('application/json')
      expect(response).to have_http_status(:ok)
    end
  end
end
